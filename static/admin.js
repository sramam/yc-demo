// declare a new module called 'myApp', and make it require the `ng-admin` module as a dependency
var myApp = angular.module('myApp', ['ng-admin']);
var entities = {};
// declare a function to run when the module bootstraps (during the 'config' phase)
myApp.config(['NgAdminConfigurationProvider', function (nga) {
  // create an admin application
  var admin = nga.application('Demo Admin')
                 .baseApiUrl('http://localhost:2016/'); // main api endpoint
  /* for each model in config.js, create an entity */
  models.map(function(model) {
    var list = [];
    var editList = [];
    // var queryList = [];
    model.list.map(function(field) {
      var type = field.type ? field.type : 'string';
      list.push(nga.field(field.name, type));
    });
    model.edit.map(function(field) {
      var type = field.type ? field.type : 'string';
      switch (type) {
        case 'reference':
          editList.push(nga.field(field.name, 'reference')
                     .targetEntity(entities[field.target])
                     .targetField(nga.field(field.field))
                     .label(field.label));
          break;
        case 'choice':
          editList.push(nga.field(field.name, 'choice')
                           .choices(field.choices));
          break;
        default:
          editList.push(nga.field(field.name, type)
                       .validation(field.validation));
          break;
      }
    });
    var entity = nga.entity(model.name);
    // set the fields of the user entity list view
    entity.listView().fields(list).listActions(['show']);;
    entity.showView().fields(editList);
    entity.creationView().fields(editList);
    entity.editionView().fields(editList);
    // add the entity to the admin application
    entities[model.name] = entity;
    admin.addEntity(entity)
  });
  // attach the admin application to the DOM and execute it
  nga.configure(admin);
}]);

myApp.config(['RestangularProvider', function (RestangularProvider) {
    RestangularProvider.addFullRequestInterceptor(function(element, operation, what, url, headers, params) {
        if (operation === "remove") {
          console.log('remove');
        }
        if (operation == "getList") {
            // custom pagination params
            if (params._page) {
                params._start = (params._page - 1) * params._perPage;
                params._end = params._page * params._perPage;
            }
            delete params._page;
            delete params._perPage;
            // custom sort params
            if (params._sortField) {
                params._sort = params._sortField;
                params._order = params._sortDir;
                delete params._sortField;
                delete params._sortDir;
            }
            // custom filters
            if (params._filters) {
                for (var filter in params._filters) {
                    params[filter] = params._filters[filter];
                }
                delete params._filters;
            }
        }
        return { params: params };
    });
}]);

myApp.config(['$httpProvider', function($httpProvider) {
        $httpProvider.defaults.useXDomain = true;
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
    }
]);
