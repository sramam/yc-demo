var Sequelize = require('sequelize'),
    epilogue = require('epilogue'),
    http = require('http'),
    express = require('express'),
    requireDir = require('require-dir'),
    bodyParser = require('body-parser'),
    morgan = require('morgan'),
    models = requireDir('./models'),
    controllers = requireDir('./controllers'),
    relations = require('./relations'),
    rp = require('request-promise');

var app, db, server, serverReady=false;

function init() {
    
  console.log('init');

  var config = {
    baseDir: process.cwd()
  };

  db = new Sequelize('database', '', '', {
      host: 'localhost',
      dialect: 'sqlite',
      storage: ':memory:'
  });
  // Define models
  var modelCache = Object.keys(models).reduce(
    function _f(acc, mname) {
      var model = models[mname](Sequelize);
      acc[mname] = db.define(model[0], model[1], model[2]); /* eslint no-param-reassign:0 */
      return acc;
    },
    {} // accumulator
  );

  // Define relations
  relations(modelCache);

  app = express();
  app.use(morgan('combined'));
  app.use(bodyParser.json({ type: 'application/json' }));
  app.use(bodyParser.urlencoded({ extended: false }));

  epilogue.initialize({
    app: app,
    sequelize: db
  });

  /* eslint vars-on-top:0 */
  for (var cname in controllers) {
    if (controllers.hasOwnProperty(cname)) {
      var ctlr = controllers[cname](
        modelCache[cname],
        config
      );
      var resource = epilogue.resource(ctlr);
      resource.use(ctlr.milestones);
    }
  }

  server = http.createServer(app);
  return db
   .sync({ force: true })
   .then(function _f() {
    server.listen(3000, function _s(err) {
      if (!err) {
	serverReady = true;
        console.log('Server started on http://localhost:' + 3000);
        return server;
      }
      return null;
    });
  });
}

exports.handler = function(event, context) {  

   var reqopts = {
      method: event.method,
      uri: 'http://127.0.0.1:3000'+event.resource,
      body: event.body,
      json: true
  };

  reqopts.uri = reqopts.uri.replace("{id}", event.id);

  var resbody = '';

  var sendReq = function() {
    rp(reqopts)
    .then(function (body) {
        context.succeed(body); 
    })
    .catch(function (err) {
        context.fail(err);
    });
  }


  if (!serverReady) {
    console.log('if');
    init();
    server.on('listening', sendReq);
  }
  else {
    console.log('else');
    sendReq();
  };
}
  

  

