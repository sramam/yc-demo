'use strict';

module.exports = function _model(DataTypes) {
  return [
    'Order_Item', {
      quantity: {
        type: "INTEGER"
      }
      ,
      price: {
        type: "STRING"
      }
    }, { // model config
    }
  ];
};
