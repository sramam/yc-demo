'use strict';

module.exports = function _model(DataTypes) {
  return [
    'Customer', {
      name: {
        type: "STRING"
      },
      phone: {
        type: "STRING"
      },
      email: {
        type: "STRING",
        validate: {
          isEmail: true
        }
      }

    }, { // model config
    }
  ];
};
