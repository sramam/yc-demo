'use strict';

module.exports = function _model(DataTypes) {
  return [
    'Order',
    {
      status: { type:"STRING" }
      , price: { type:"STRING" }
      , date: { type:"DATE" }
    },
    { // model config
    }
  ];
};

