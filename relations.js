'use strict';

module.exports = function _relations(models) {
  models.Supplier.hasMany(models.Product);
  models.Product.hasMany(models.Order_Item);
  models.Order.hasMany(models.Order_Item);
};
