'use strict';

module.exports = function controller(model, config) {
  return {
    model: model,
    endpoints: ['/customers', '/customers/:id'],
    actions: ['list', 'create', 'read', 'update', 'delete'],
    excludeAttributes: []
  };
};

