'use strict';

module.exports = function controller(model, config) {
  return {
    model: model,
    endpoints: ['/products', '/products/:id'],
    actions: ['list', 'create', 'read', 'update', 'delete'],
    excludeAttributes: []
  };
};


