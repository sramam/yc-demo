'use strict';

module.exports = function controller(model, config) {
  return {
    model: model,
    endpoints: ['/order_items', '/order_items/:id'],
    actions: ['list', 'create', 'read', 'update', 'delete'],
    excludeAttributes: []
  };
};


