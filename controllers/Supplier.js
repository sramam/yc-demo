'use strict';

module.exports = function controller(model, config) {
  return {
    model: model,
    endpoints: ['/suppliers', '/suppliers/:id'],
    actions: ['list', 'create', 'read', 'update', 'delete'],
    excludeAttributes: []
  };
};


