'use strict';

module.exports = function controller(model, config) {
  return {
    model: model,
    endpoints: ['/orders', '/orders/:id'],
    actions: ['list', 'create', 'read', 'update', 'delete'],
    excludeAttributes: []
  };
};


